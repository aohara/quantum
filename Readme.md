Quantum - Universal S3 Explorer
===============================

A cross-platform AWS S3 Explorer, written with Java Swing.

Requirements
------------

- Java Runtime Environment 8 or greater from [Oracle](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) or [OpenJdk](https://openjdk.java.net/install/index.html)

