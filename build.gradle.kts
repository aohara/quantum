import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "io.andrewohara"
version = "1.0-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.3.31"
    id("application")
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

dependencies {
    implementation("com.amazonaws:aws-java-sdk-s3:1.11.562")
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.ocpsoft.prettytime:prettytime:4.0.1.Final")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.3.1")
    testImplementation("io.findify:s3mock_2.12:0.2.4")
    testImplementation("org.hamcrest:hamcrest-library:2.1")
}

repositories {
    mavenCentral()
}

application {
    mainClassName = "io.andrewohara.quantum.Quantum"
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform { }
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
