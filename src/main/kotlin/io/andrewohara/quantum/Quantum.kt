package io.andrewohara.quantum

import com.amazonaws.services.s3.AmazonS3ClientBuilder
import io.andrewohara.quantum.controllers.DirectoryController
import io.andrewohara.quantum.models.S3Bucket
import io.andrewohara.quantum.views.MainWindow
import javax.swing.SwingUtilities

class Quantum {
    companion object {
        private const val name = "Quantum - Universal S3 Explorer"

        @JvmStatic
        fun main(args: Array<String>) {
            SwingUtilities.invokeLater {
                val client = AmazonS3ClientBuilder.defaultClient()
                val controller = DirectoryController(client)
                val window = MainWindow(name, controller)
                window.isVisible = true

                Thread { controller.refresh() }.start()
            }
        }
    }
}