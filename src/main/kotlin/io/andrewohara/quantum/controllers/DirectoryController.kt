package io.andrewohara.quantum.controllers

import com.amazonaws.services.s3.AmazonS3
import io.andrewohara.quantum.models.S3Bucket
import io.andrewohara.quantum.models.S3Directory
import io.andrewohara.quantum.models.S3File

class DirectoryController(private val client: AmazonS3): FileListener {

    val observers: MutableSet<DirectoryListener> = mutableSetOf()

    private var buckets: List<S3Bucket> = emptyList()
    private var directory: S3Directory? = null
    private var clipboard: Clipboard? = null

    fun setDirectory(next: S3Directory?) {
        directory = next
        observers.forEach { it.setDirectory(buckets, directory) }
    }

    private fun updateBuckets(newBuckets: List<S3Bucket>) {
        this.buckets = newBuckets
        observers.forEach { it.setDirectory(buckets, directory) }
    }

    fun refresh() {
        observers.forEach { it.startUpdate() }
        val buckets: List<S3Bucket> = client.listBuckets()
                .map { S3Bucket(client, it) }
        directory?.refresh()
        updateBuckets(buckets)
    }

    fun copy(file: S3File) {
        clipboard = Clipboard(file, false)
    }

    fun cut(file: S3File) {
        clipboard = Clipboard(file, true)
    }

    fun paste() {
        val source = clipboard ?: return
        val destDir = directory ?: return

        destDir.copy(source.file)
        if (source.cut) {
            source.file.delete()
        }

        source.file.parent.refresh()
        refresh()
        clipboard = null
    }

    fun hasClipboard() = clipboard != null

    override fun onDeleted(file: S3File) {
        refresh()
    }

    private data class Clipboard(val file: S3File, val cut: Boolean)
}