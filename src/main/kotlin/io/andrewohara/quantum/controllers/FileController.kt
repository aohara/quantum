package io.andrewohara.quantum.controllers

import io.andrewohara.quantum.models.S3File

class FileController(private val file: S3File, listener: DirectoryController) {

    val listeners: MutableSet<FileListener> = mutableSetOf(listener)

    fun update(text: String) {
        text.byteInputStream().use { content ->
            file.updateContent(content, text.length.toLong())
        }
        listeners.forEach { it.onUpdated(file) }
    }

    fun delete() {
        file.delete()
        listeners.forEach { it.onDeleted(file) }
    }

    fun refresh() {
        listeners.forEach { it.onUpdated(file) }
    }
}