package io.andrewohara.quantum.controllers

import io.andrewohara.quantum.models.S3Bucket
import io.andrewohara.quantum.models.S3Directory
import io.andrewohara.quantum.models.S3File

interface DirectoryListener {
    fun startUpdate() { }
    fun setDirectory(buckets: List<S3Bucket>, directory: S3Directory?) { }
}

interface FileListener {
    fun onUpdated(file: S3File) { }
    fun onDeleted(file: S3File) { }
}