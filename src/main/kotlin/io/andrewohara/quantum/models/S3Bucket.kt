package io.andrewohara.quantum.models

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.Bucket
import java.nio.file.Path
import java.nio.file.Paths

class S3Bucket(private val client: AmazonS3, bucket: Bucket): S3Element {

    override val name: String = bucket.name
    override val path: Path = Paths.get("")
    override val parent: S3Element? = null

    fun directory(): S3Directory = S3Directory(client, this, this, name)

    override fun toString() = "$name/"
}