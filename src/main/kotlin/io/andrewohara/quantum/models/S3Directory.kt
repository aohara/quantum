package io.andrewohara.quantum.models

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.*
import java.io.InputStream
import java.nio.file.Path
import java.nio.file.Paths

class S3Directory(
        private val client: AmazonS3,
        val bucket: S3Bucket,
        override val parent: S3Element,
        override val name: String
): S3Element {
    private var summary: ListObjectsV2Result? = null

    override val path: Path by lazy {
        parent.path.resolve(name)
    }

    private val relPath by lazy {
        val pathBase = Paths.get(bucket.name)
        pathBase.relativize(path)
    }

    fun directory(name: String): S3Directory? = directories()
            .find { it.name == name }

    fun directories(): List<S3Directory> = summary().commonPrefixes
            .map { Paths.get(it).fileName.toString() }
            .map {
                S3Directory(client, bucket, this, it)
            }

    fun files() = summary().objectSummaries.map { S3File(client, this, it) }
    fun file(filename: String): S3File? = files()
            .find { it.name == filename }

    fun contents(): List<S3Element> = directories() + files()

    fun isRoot() = parent is S3Bucket

    fun putObject(filename: String, content: InputStream, contentType: String, contentLength: Long): S3File {
        val metadata = ObjectMetadata().apply {
            this.contentType = contentType
            this.contentLength = contentLength
        }

        val key = relPath.resolve(filename).toString()
        client.putObject(bucket.name, key, content, metadata)
        return file(filename)!!
    }

    fun copy(file: S3File) {
        client.copyObject(file.parent.bucket.name, file.key, bucket.name, relPath.resolve(file.name).toString())
    }

    private fun summary(): ListObjectsV2Result {
        if (summary == null) {
            val request = ListObjectsV2Request()
                    .withBucketName(bucket.name)
                    .withPrefix(if (relPath.toString().isBlank()) "" else "$relPath/")
                    .withDelimiter("/")
            summary = client.listObjectsV2(request)
        }
        return summary!!
    }

    fun refresh() {
        summary = null
    }

    override fun toString() = "$name/"
    override fun equals(other: Any?) = if (other is S3Directory) path == other.path else false
    override fun hashCode() = path.hashCode()
}
