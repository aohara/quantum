package io.andrewohara.quantum.models

import java.nio.file.Path

interface S3Element {

    val parent: S3Element?
    val name: String
    val path: Path
}