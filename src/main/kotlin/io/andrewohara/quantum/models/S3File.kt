package io.andrewohara.quantum.models

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.S3ObjectSummary
import java.io.InputStream
import java.nio.file.Path
import java.nio.file.Paths

class S3File(
        private val client: AmazonS3,
        override val parent: S3Directory,
        private val summary: S3ObjectSummary
): S3Element {

    override val name: String by lazy { Paths.get(key).fileName.toString() }
    override val path: Path by lazy { parent.path.resolve(name) }
    private var metadata: ObjectMetadata? = null
    val key: String by lazy { summary.key }


    override fun toString() = name

    fun content(): InputStream = client.getObject(summary.bucketName, key).objectContent

    fun updateContent(content: InputStream, length: Long) {
        val metadata = ObjectMetadata().apply {
            contentType = metadata().contentType
            contentLength = length
        }
        val result = client.putObject(summary.bucketName, key, content, metadata)
        this.metadata = result.metadata
    }

    fun delete() {
        client.deleteObject(summary.bucketName, summary.key)
    }

    fun metadata(): ObjectMetadata {
        if (metadata == null) {
            metadata = client.getObject(summary.bucketName, key).objectMetadata
        }
        return metadata!!
    }
}