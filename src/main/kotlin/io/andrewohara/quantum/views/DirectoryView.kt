package io.andrewohara.quantum.views

import io.andrewohara.quantum.controllers.DirectoryController
import io.andrewohara.quantum.controllers.DirectoryListener
import io.andrewohara.quantum.controllers.FileController
import io.andrewohara.quantum.models.S3Bucket
import io.andrewohara.quantum.models.S3Directory
import io.andrewohara.quantum.models.S3Element
import io.andrewohara.quantum.models.S3File
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*

class DirectoryView(private val controller: DirectoryController): JList<S3Element>(), DirectoryListener {

    private val listModel = DefaultListModel<S3Element>()
    private val pasteItem: JMenuItem = JMenuItem("Paste")
    private val contextMenu = JPopupMenu()

    private fun MouseEvent.selected(): S3Element = listModel[locationToIndex(point)]

    init {
        model = listModel

        addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(e: MouseEvent) {
                handleClick(e)
            }
        })

        // TODO add clipboard controller and COntextMenu class that listens for clipboard events (for when to enable/disable paste item)
        val copyItem = JMenuItem("Copy")
        copyItem.addActionListener {
            val selected = selectedValue
            if (selected is S3File) {
                controller.copy(selected)
            }
        }
        contextMenu.add(copyItem)

        val cutItem = JMenuItem("Cut")
        cutItem.addActionListener {
            val selected = selectedValue
            if (selected is S3File) {
                controller.cut(selected)
            }
        }
        contextMenu.add(cutItem)

        pasteItem.addActionListener {
            controller.paste()
        }
        contextMenu.add(pasteItem)


        val deleteItem = JMenuItem("Delete")
        deleteItem.addActionListener {
            val selected = selectedValue
            if (selected is S3File) {
                val fileController = FileController(selected, controller)
                fileController.delete()
            }
        }
        contextMenu.add(deleteItem)
    }

    private fun handleClick(e: MouseEvent) {
        if (e.clickCount == 2) {
            enter(e)
        } else if (SwingUtilities.isRightMouseButton(e)) {
            setSelectedValue(e.selected(), false)
            openMenu(e)
        }
    }

    override fun setDirectory(buckets: List<S3Bucket>, directory: S3Directory?) {
        listModel.removeAllElements()

        if (directory != null) {
            for (item in directory.contents()) {
                listModel.addElement(item)
            }
        } else {
            for (bucket in buckets) {
                listModel.addElement(bucket)
            }
        }
    }

    private fun enter(e: MouseEvent) {
        when (val selected = e.selected()) {
            is S3Directory -> controller.setDirectory(selected)
            is S3File -> S3FileView(FileController(selected, controller))
            is S3Bucket -> controller.setDirectory(selected.directory())
        }
    }

    private fun openMenu(e: MouseEvent) {
        pasteItem.isEnabled = controller.hasClipboard()

        contextMenu.show(this, e.x, e.y)
    }
}