package io.andrewohara.quantum.views

import io.andrewohara.quantum.controllers.DirectoryController
import io.andrewohara.quantum.controllers.DirectoryListener
import io.andrewohara.quantum.models.S3Bucket
import io.andrewohara.quantum.models.S3Directory
import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.*

class MainWindow(name: String, controller: DirectoryController): JFrame(name), DirectoryListener {

    private val spinner = JProgressBar().apply {
        isIndeterminate = true
    }

    init {
        defaultCloseOperation = EXIT_ON_CLOSE
        size = Dimension(800, 600)

        val toolbar = QuantumToolbar(controller)
        contentPane.add(toolbar, BorderLayout.PAGE_START)
        controller.observers.add(toolbar)

        contentPane.add(spinner, BorderLayout.AFTER_LAST_LINE)
        controller.observers.add(this)

        val dirView = DirectoryView(controller)
        contentPane.add(dirView, BorderLayout.CENTER)
        controller.observers.add(dirView)
    }

    override fun startUpdate() {
        spinner.isVisible = true
    }

    override fun setDirectory(buckets: List<S3Bucket>, directory: S3Directory?) {
        spinner.isVisible = false
    }
}