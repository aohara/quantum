package io.andrewohara.quantum.views

import io.andrewohara.quantum.controllers.DirectoryController
import io.andrewohara.quantum.controllers.DirectoryListener
import io.andrewohara.quantum.models.S3Bucket
import io.andrewohara.quantum.models.S3Directory
import io.andrewohara.quantum.models.S3Element
import javax.swing.*

class QuantumToolbar(private val controller: DirectoryController): JToolBar(), DirectoryListener {

    private val pathCrumbs = JPanel()

    init {
        val refreshButton = JButton("Refresh")
        refreshButton.addActionListener {
            controller.refresh()
        }
        add(refreshButton)

        add(pathCrumbs)
    }

    override fun startUpdate() {
        // no-op
    }

    override fun setDirectory(buckets: List<S3Bucket>, directory: S3Directory?) {
        val history = mutableListOf<S3Directory>()
        directory?.let {
            var cur: S3Element = it
            history.add(it)

            while (cur.parent is S3Directory) {
                history.add(cur.parent as S3Directory)
                cur = cur.parent as S3Directory
            }
        }

        pathCrumbs.removeAll()

        val rootButton = JButton("root")
        rootButton.addActionListener {
            controller.setDirectory(null)
        }
        pathCrumbs.add(rootButton)
        pathCrumbs.add(JLabel("/"))

        history.reversed().forEach { dir ->
            val segmentButton = JButton(dir.name)
            segmentButton.addActionListener {
                controller.setDirectory(dir)
            }
            pathCrumbs.add(segmentButton)
            pathCrumbs.add(JLabel("/"))
        }

        revalidate()
        repaint()
    }
}