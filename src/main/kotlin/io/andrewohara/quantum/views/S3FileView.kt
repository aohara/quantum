package io.andrewohara.quantum.views

import io.andrewohara.quantum.controllers.FileController
import io.andrewohara.quantum.controllers.FileListener
import io.andrewohara.quantum.models.S3File
import org.ocpsoft.prettytime.PrettyTime
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*

class S3FileView(private val controller: FileController): JDialog(), FileListener {

    private val editor = JEditorPane()
    private val lastModified = JLabel()
    private val timeFormat = PrettyTime()

    init {
        val toolbar = JToolBar()

        val closeButton = JButton("Close")
        closeButton.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(e: MouseEvent?) {
                handleClose()
            }
        })
        toolbar.add(closeButton)

        val saveButton = JButton("Save")
        saveButton.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(e: MouseEvent?) {
               handleUpdate()
            }
        })
        toolbar.add(saveButton)

        add(toolbar, BorderLayout.PAGE_START)
        editor.text = "Loading..."
        add(JScrollPane(editor), BorderLayout.CENTER)

        val statusBar = JToolBar()
        statusBar.add(lastModified)
        add(statusBar, BorderLayout.PAGE_END)

        size = Dimension(800, 600)
        isVisible = true

        controller.listeners.add(this)
        controller.refresh()
    }

    private fun handleClose() {
        dispose()
    }

    private fun handleUpdate() {
        controller.update(editor.text)
    }

    override fun onUpdated(file: S3File) {
        val timeAgo = timeFormat.format(file.metadata().lastModified)
        lastModified.text = "Last Modified $timeAgo"
        editor.read(file.content(), file.metadata().contentType)
    }

    override fun onDeleted(file: S3File) {
        dispose()
    }
}