package io.andrewohara.quantum.models

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.AnonymousAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import io.findify.s3mock.S3Mock

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.*
import java.nio.file.Paths


class S3DirectoryUnitTest {

    companion object {
        private val api = S3Mock.Builder().withPort(8001).withInMemoryBackend().build()

        @JvmStatic
        @BeforeAll
        fun beforeAll() {
            api.start()
        }

        @JvmStatic
        @AfterAll
        fun afterAll() {
            api.stop()
        }
    }

    private lateinit var client: AmazonS3
    private lateinit var bucket: S3Bucket
    private lateinit var root: S3Directory

    @BeforeEach
    fun beforeEach() {
        val endpoint = AwsClientBuilder.EndpointConfiguration("http://localhost:8001", "ca-central-1")
        client = AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpoint)
                .withCredentials(AWSStaticCredentialsProvider(AnonymousAWSCredentials()))
                .build()
        bucket = S3Bucket(client, client.createBucket("bucket"))
        client.putObject(bucket.name, "foo.txt", "foo")
        client.putObject(bucket.name, "bar.txt", "bar")
        client.putObject(bucket.name, "toll/troll.txt", "troll")
        client.putObject(bucket.name, "toll/trolls.txt", "trolls")
        client.putObject(bucket.name, "static/img/photo.jpg", "jpegstuff")
        client.putObject(bucket.name, "static/js/script.js", "console.log('hi')")

        root = bucket.directory()
    }

    @AfterEach
    fun afterEach() {
        client.deleteBucket(bucket.name)
    }


    @Test
    fun root() {
        assertThat(root.name, equalTo(bucket.name))
        assertThat(root.path.toString(), equalTo(Paths.get(bucket.name).toString()))
        assertThat(root.isRoot(), equalTo(true))

        assertThat(root.files().map { it.name }, contains("bar.txt", "foo.txt"))
        assertThat(root.directories().map { it.path.toString() }, contains("bucket/static", "bucket/toll"))
        assertThat(root.contents().map { it.name }, containsInAnyOrder("foo.txt", "bar.txt", "toll", "static"))
    }

    @Test
    fun rootFooFile() {
        val file = root.file("foo.txt")!!
        assertThat(file.name, equalTo("foo.txt"))
        assertThat(file.path.toString(), equalTo("bucket/foo.txt"))
        assertThat(file.content().reader().readText(), equalTo("foo"))
    }

    @Test
    fun rootMissingFile() {
        val file = root.file("lol.cats")
        assertThat(file, nullValue())
    }

    @Test
    fun missingDir() {
        val dir = root.directory("foo")
        assertThat(dir, nullValue())
    }

    @Test
    fun tollDir() {
        val tollDir = root.directory("toll")!!

        assertThat(tollDir.name, equalTo("toll"))
        assertThat(tollDir.path.toString(), equalTo("bucket/toll"))
        assertThat(tollDir.isRoot(), equalTo(false))
        assertThat(tollDir.parent.name, equalTo("bucket"))

        assertThat(tollDir.files().map { it.name }, containsInAnyOrder("troll.txt", "trolls.txt"))
        assertThat(tollDir.directories(), empty())
        assertThat(tollDir.contents().map { it.path.toString() }, contains("bucket/toll/troll.txt", "bucket/toll/trolls.txt"))
    }

    @Test
    fun tollDirFile() {
        val file = root.directory("toll")!!.file("troll.txt")!!

        assertThat(file.name, equalTo("troll.txt"))
        assertThat(file.path.toString(), equalTo("bucket/toll/troll.txt"))
        assertThat(file.content().reader().readText(), equalTo("troll"))
    }

    @Test
    fun staticDir() {
        val dir = root.directory("static")!!
        assertThat(dir.name, equalTo("static"))
        assertThat(dir.path.toString(), equalTo("bucket/static"))
        assertThat(dir.parent.name, equalTo("bucket"))

        assertThat(dir.files(), empty())
        assertThat(dir.directories().map { it.name }, containsInAnyOrder("img", "js"))
        println(dir.directories().map { it.path.toString() })
        assertThat(dir.directories().map { it.path.toString() }, containsInAnyOrder("bucket/static/img", "bucket/static/js"))
    }

    @Test
    fun staticImgDir() {
        val dir = root.directory("static")?.directory("img")!!
        assertThat(dir.name, equalTo("img"))
        assertThat(dir.path.toString(), equalTo("bucket/static/img"))
        assertThat(dir.parent.name, equalTo("static"))

        assertThat(dir.directories().map { it.path }, empty())
        assertThat(dir.files().map { it.name }, containsInAnyOrder("photo.jpg"))
    }
}