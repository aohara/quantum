package io.andrewohara.quantum.models

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.AnonymousAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import io.findify.s3mock.S3Mock
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*

class S3FileUnitTest {
    companion object {
        private val api = S3Mock.Builder().withPort(8002).withInMemoryBackend().build()

        @JvmStatic
        @BeforeAll
        fun beforeAll() {
            api.start()
        }

        @JvmStatic
        @AfterAll
        fun afterAll() {
            api.stop()
        }
    }

    private lateinit var client: AmazonS3
    private lateinit var bucket: S3Bucket
    private lateinit var file: S3File

    @BeforeEach
    fun beforeEach() {
        val endpoint = AwsClientBuilder.EndpointConfiguration("http://localhost:8002", "ca-central-1")
        client = AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpoint)
                .withCredentials(AWSStaticCredentialsProvider(AnonymousAWSCredentials()))
                .build()
        bucket = S3Bucket(client, client.createBucket("bucket"))

        "foo".byteInputStream().use { content ->
            file = bucket.directory().putObject("foo.txt", content, "text/plain", "foo".length.toLong())
        }
    }

    @Test
    fun create() {
        val file: S3File = "lol".byteInputStream().use {
            bucket.directory().putObject("lol.txt", it, "text/plain", "lol".length.toLong())
        }
        assertThat(file.name, equalTo("lol.txt"))
        assertThat(file.path.toString(), equalTo("bucket/lol.txt"))
        assertThat(file.parent, equalTo(bucket.directory()))
    }

    @Test
    fun update() {
        val text = "foobar"
        val updated = text.byteInputStream().use {
            file.updateContent(it, text.length.toLong())
            file
        }

        val result = client.getObject(bucket.name, "foo.txt")
        val content = result.objectContent.bufferedReader().readText()
        assertThat(content, equalTo("foobar"))

        assertThat(updated.content().reader().readText(), equalTo("foobar"))
    }

    @Test fun delete() {
        file.delete()
        assertThat(bucket.directory().file(file.name), nullValue())
    }
}